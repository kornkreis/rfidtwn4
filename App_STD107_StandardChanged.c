// ******************************************************************
//
//    File: App_STD107_Standard.c
//    Date: 2014-09-09
// Version: 1.07
//
// Purpose:
//
// This is the standard app for TWN4 readers, which is installed
// as default app on TWN4 readers. This script can be run on any
// type of TWN4 reader without modification.
// 
// Feel free to modify this program for your specific purposes!
//
// V1:
// ------
// - Initial release
//
// V2: 
// ---
// - Support configuration via AppBlaster
//
// V1.03: 
// ------
// - Changed version numbering scheme
//
// V1.04: 
// ------
// - Configure tags being searched for only if LFTAGTYPES or
//   HFTAGTYPES requests a specific type. Otherwise, default setup
//   by firmware is used.
// - Marked as template for AppBlaster
// - Support ICLASS option
// - Change style how options are evaluated more convenient
//
// V1.05: 
// ------
// - NFCP2P Demo Support
//
// V1.06:
// ------
// - Adaption to reworked NFC SNEP service
//
// V1.07:
// ------
// - Adaption to new naming scheme of functions for host communication
// - iCLASS: Read UID by default instead of PAC
//
// ******************************************************************

#include "twn4.sys.h"
#include "apptools.h"

#define MAXIDBYTES  10
#define MAXIDBITS   (MAXIDBYTES*8)

#define ACCESS_ALLOWED 0x05

byte ID[MAXIDBYTES];
int IDBitCnt;
int TagType;

byte LastID[MAXIDBYTES];
int LastIDBitCnt;
int LastTagType;

const unsigned char AppManifest[] = { EXECUTE_APP, 1, EXECUTE_APP_ALWAYS, TLV_END };

// ******************************************************************
// ****** Section containing configurable behaviour *****************
// ******************************************************************

// If necessary adjust default parameters
#ifndef __APP_CONFIG_H__
    #define LFTAGTYPES        		0
    #define HFTAGTYPES        		0
    #define BIT_LENGTHS       		0xFFFFFFFFFFFFFFFF
    #define SWAP_BITS         		0
    #define SWAP_BYTES        		0
    #define OUTPUT_ALL_BITS   		1
    #define BITOFFSET         		0
    #define BITCOUNT          		0
    #define OUTPUT_RADIX      		16
    #define OUTPUT_DIGITS     		0
    #define ICLASS_READMODE_VALUE  	ICLASS_READMODE_UID
#endif


// ******************************************************************


void ConfigWriteID(void)
{
	if (OUTPUT_DIGITS == 0 && OUTPUT_RADIX == 2)
	{
	    // Make number of digits identical with number of digits
	    HostWriteRadix(ID,IDBitCnt,IDBitCnt,OUTPUT_RADIX);
	}
	else if (OUTPUT_DIGITS == 0 && OUTPUT_RADIX == 16)
	{
	    // Make number of digits always a multiple of two (= one byte),
	    // where all bits fit into
	    HostWriteRadix(ID,IDBitCnt,(IDBitCnt+7)/8*2,OUTPUT_RADIX);
	}
	else
	    HostWriteRadix(ID,IDBitCnt,OUTPUT_DIGITS,OUTPUT_RADIX);
}

int main(void)
{

	// *** RELAS GmbH ***
    GPIOConfigureOutputs(GPIO2|GPIO3,GPIO_PUPD_PULLDOWN,GPIO_OTYPE_PUSHPULL);
	GPIOClearBits(GPIO2|GPIO3);
    
	I2CInit(I2CMODE_SLAVE | 0x30 | I2CMODE_CHANNEL);
	SetHostChannel(CHANNEL_I2C);
	// *** ***
	
    // Init LEDs
    LEDInit(REDLED | GREENLED);
    // Turn on green LED
    LEDOn(GREENLED);
    // Turn off red LED
    LEDOff(REDLED);
    // Make some noise at startup at low volume
    SetVolume(30);
    BeepLow();
    BeepHigh();
    // Continue with maximum  volume
    SetVolume(100);
    
    // No transponder found up to now
    LastTagType = NOTAG;
	byte answer;

	while (true)
    {
        // Search a transponder
        if (SearchTag(&TagType,&IDBitCnt,ID,sizeof(ID)))
        {
			// Is this transponder new to us?
			if (TagType != LastTagType || IDBitCnt != LastIDBitCnt || !CompBits(ID,0,LastID,0,MAXIDBITS))
			{
				// to clean host-buffer
				while (HostTestByte())
					answer = HostReadByte();
				
				// Save this as known ID
				CopyBits(LastID,0,ID,0,MAXIDBITS);
				LastIDBitCnt = IDBitCnt;
				LastTagType = TagType;
                
				// Hardware communication
				BeepHigh();
				LEDOff(GREENLED);
				LEDOn(REDLED);
				
				// Transmit ID to Host
				for (int i = 0; i < sizeof(ID); i++)
					HostWriteByte(ID[i]);//HostWriteHex(ID,IDBitCnt,(IDBitCnt+7)/8*2);
				
				//
				/* OR this:*/
				//ConfigWriteID();
				
				// set IO Bits - motherboard will now start communication - read 10 bytes
				GPIOSetBits(GPIO2|GPIO3);
				
				// wait for answer
				while (!HostTestByte()){}
				answer = HostReadByte();
				
				// if answer is "access allowed"
				if (answer == ACCESS_ALLOWED)
				{
					LEDOn(GREENLED);
					BeepLow();
				}
				
				// clear IO
				GPIOClearBits(GPIO2|GPIO3);
			}
        }
    }
}
