// ******************************************************************
//
// Purpose:
//
// App for TWN4, which implements the "Simple Protocol".
//
// V1:
// ------
// - Initial release
//
// V1.01: 
// ------
// - Changed version numbering scheme
// - Split App into several modules
//
// V1.02:
// ------
// - Abstraction layer for support of binary modes
//
// V1.03:
// ------
// - Support optional use of CRC
// - Renamed functions for simple protocol
// - Split protocol handler into TestCommand/ExecuteCommand/SendResponse
// - External access to received messages
// 
// V1.04:
// ------
// - System functions as provided by firmware V1.64
//
// ******************************************************************
//
//      Reference messages for function GetUSBType
//
//      -------------------------------------------------------------------------------
//      Mode      CRC     Command (Host -> TWN4)          Response (TWN4 -> Host)
//      -------------------------------------------------------------------------------
//      ASCII     Off     "0005\r"                        "0001\r"
//      ASCII     On      "000515A7\r"                    "000131E1\r"
//      Binary    Off     0x02 0x00 0x00 0x05             0x02 0x00 0x00 0x01
//      Binary    On      0x04 0x00 0x00 0x05 0x15 0xA7   0x04 0x00 0x00 0x01 0x31 0xE1
//   
//      Reference implementations of CRC algorithm
//      -------------------------------------------------------------------------------
//      Language C
//      ----------
//
//  	uint16_t UpdateCRC(uint16_t CRC,byte Byte)
//  	{
//      	// Update CCITT CRC (reverse polynom 0x8408)
//      	Byte ^= (byte)CRC;
//      	Byte ^= (byte)(Byte << 4);
//      	return (uint16_t)(((Byte << 8) | (CRC >> 8)) ^ (Byte >> 4) ^ (Byte << 3));
//  	}
//
//      Language C#
//      -----------
//
//      ushort UpdateCRC(ushort CRC,byte Byte)
//      {
//      	// Update CCITT CRC (reverse polynom 0x8408)
//          Byte ^= (byte)CRC;
//          Byte ^= (byte)(Byte << 4);
//          return (ushort)(((Byte << 8) | (CRC >> 8)) ^ (Byte >> 4) ^ (Byte << 3));
//      }
//
// ******************************************************************

#include "twn4.sys.h"
#include "apptools.h"
#include "prs.h"

#define MAXIDBYTES  10
#define MAXIDBITS   (MAXIDBYTES*8)

byte ID[MAXIDBYTES];
int IDBitCnt;
int TagType;

byte LastID[MAXIDBYTES];
int LastIDBitCnt;
int LastTagType;

const unsigned char AppManifest[] = { EXECUTE_APP, 1, EXECUTE_APP_ALWAYS, TLV_END };

// ******************************************************************
// ****** Main Program **********************************************
// ******************************************************************

int main(void)
{
	
	
	// Init LEDs
    LEDInit(REDLED | GREENLED);
    
    SetVolume(30);
	SetHostChannel(CHANNEL_I2C);
	I2CInit(I2CMODE_SLAVE | 0x30 | I2CMODE_CHANNEL);
	
	BeepHigh();
	BeepLow();
	LEDOn(REDLED);
	//END OF INSERT
	
    // Main loop
    while (true)
    {
		if (SearchTag(&TagType,&IDBitCnt,ID,sizeof(ID)))
		{
			if (TagType != LastTagType || IDBitCnt != LastIDBitCnt || !CompBits(ID,0,LastID,0,MAXIDBITS))
			{
				CopyBits(LastID,0,ID,0,MAXIDBITS);
				LastIDBitCnt = IDBitCnt;
				LastTagType = TagType;

				BeepHigh();
			}
				
		}
    }
}
