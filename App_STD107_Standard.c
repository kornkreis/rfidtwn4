// ******************************************************************
//
//    File: App_STD107_Standard.c
//    Date: 2014-09-09
// Version: 1.07
//
// Purpose:
//
// This is the standard app for TWN4 readers, which is installed
// as default app on TWN4 readers. This script can be run on any
// type of TWN4 reader without modification.
// 
// Feel free to modify this program for your specific purposes!
//
// V1:
// ------
// - Initial release
//
// V2: 
// ---
// - Support configuration via AppBlaster
//
// V1.03: 
// ------
// - Changed version numbering scheme
//
// V1.04: 
// ------
// - Configure tags being searched for only if LFTAGTYPES or
//   HFTAGTYPES requests a specific type. Otherwise, default setup
//   by firmware is used.
// - Marked as template for AppBlaster
// - Support ICLASS option
// - Change style how options are evaluated more convenient
//
// V1.05: 
// ------
// - NFCP2P Demo Support
//
// V1.06:
// ------
// - Adaption to reworked NFC SNEP service
//
// V1.07:
// ------
// - Adaption to new naming scheme of functions for host communication
// - iCLASS: Read UID by default instead of PAC
//
// ******************************************************************

#include "twn4.sys.h"
#include "apptools.h"

#define MAXIDBYTES  10
#define MAXIDBITS   (MAXIDBYTES*8)

byte ID[MAXIDBYTES];
int IDBitCnt;
int TagType;

byte LastID[MAXIDBYTES];
int LastIDBitCnt;
int LastTagType;

// ******************************************************************
// ****** Section containing configurable behaviour *****************
// ******************************************************************

// If necessary adjust default parameters
#ifndef __APP_CONFIG_H__
    #define LFTAGTYPES        		0
    #define HFTAGTYPES        		0
    #define BIT_LENGTHS       		0xFFFFFFFFFFFFFFFF
    #define SWAP_BITS         		0
    #define SWAP_BYTES        		0
    #define OUTPUT_ALL_BITS   		1
    #define BITOFFSET         		0
    #define BITCOUNT          		0
    #define OUTPUT_RADIX      		16
    #define OUTPUT_DIGITS     		0
    #define ICLASS_READMODE_VALUE  	ICLASS_READMODE_UID
#endif

// Adjust parameters as configured in AppBlaster
const byte TLVBytes[] = { ICLASS_READMODE, 1, ICLASS_READMODE_VALUE, TLV_END };
void ConfigSetParameters(void)
{
	SetParameters(TLVBytes,sizeof(TLVBytes));
}

// Setup tag types as configured in AppBlaster
void ConfigSetTagTypes(void)
{
	if (LFTAGTYPES || HFTAGTYPES)
    	SetTagTypes(LFTAGTYPES,HFTAGTYPES);
}

// Test: Is ID length according to configured in AppBlaster?
bool ConfigTestIDLength(void)
{
	unsigned long long BitMask = 1ULL << (IDBitCnt-1);
	return (BitMask & BIT_LENGTHS) != 0;
}

// Modify ID as configured in AppBlaster
void ConfigModifyID(void)
{
	if (SWAP_BITS)
    	SwapBits(ID,0,IDBitCnt);
	if (SWAP_BYTES)
    	SwapBytes(ID,(IDBitCnt+7)/8);
	if (!OUTPUT_ALL_BITS)
	{
    	CopyBits(ID,0,ID,BITOFFSET,BITCOUNT);
    	IDBitCnt = BITCOUNT;
    }
}

// Send ID to host as configured in AppBlaster
void ConfigWriteID(void)
{
	if (OUTPUT_DIGITS == 0 && OUTPUT_RADIX == 2)
	{
	    // Make number of digits identical with number of digits
	    HostWriteRadix(ID,IDBitCnt,IDBitCnt,OUTPUT_RADIX);
	}
	else if (OUTPUT_DIGITS == 0 && OUTPUT_RADIX == 16)
	{
	    // Make number of digits always a multiple of two (= one byte),
	    // where all bits fit into
	    HostWriteRadix(ID,IDBitCnt,(IDBitCnt+7)/8*2,OUTPUT_RADIX);
	}
	else
	    HostWriteRadix(ID,IDBitCnt,OUTPUT_DIGITS,OUTPUT_RADIX);
}	    

// ******************************************************************
// ****** NFC-Specific Stuff ****************************************
// ******************************************************************

typedef struct
{
	byte* Header;
	byte TypeLength;
	int PayloadLength;
	byte IDLength;
	byte* Type;
	byte* ID;
	byte* Payload;
	byte* NextRecord;
} TNDEFRecord;

#define NDEF_IsMBSet(Header)	(Header & 0x80)
#define NDEF_IsMESet(Header)	(Header & 0x40)
#define NDEF_IsCFSet(Header)	(Header & 0x20)
#define NDEF_IsSRSet(Header)	(Header & 0x10)
#define NDEF_IsILSet(Header)	(Header & 0x08)
#define NDEF_GetTNF(Header)		(Header & 0x07)

// Find position of payload, the NDEF message itself
void NDEF_GetRecord(byte* Msg, TNDEFRecord* Rec)
{
	byte* pMsg;

	Rec->Header = Msg;
	Rec->TypeLength = Msg[1];
	if (NDEF_IsSRSet(*Rec->Header))
	{
		Rec->PayloadLength = Msg[2];
		pMsg = &Msg[3];
	}
	else
	{
		Rec->PayloadLength = Msg[5];
		Rec->PayloadLength |= Msg[4] << 8;
		Rec->PayloadLength |= Msg[3] << 16;
		Rec->PayloadLength |= Msg[2] << 24;
		pMsg = &Msg[6];
	}
	if (NDEF_IsILSet(*Rec->Header))
		Rec->IDLength = *pMsg++;
	else
		Rec->IDLength = 0;
	Rec->Type = Rec->TypeLength ? pMsg : NULL;
	pMsg += Rec->TypeLength;
	Rec->ID = Rec->IDLength ? pMsg : NULL;
	pMsg += Rec->IDLength;
	Rec->Payload = Rec->PayloadLength ? pMsg : NULL;
	pMsg += Rec->PayloadLength;
	Rec->NextRecord = NDEF_IsMESet(*Rec->Header) ? NULL : pMsg;
}

void ReceiveNDEFMessages(void)
{
	// Wait for SNEP service is running
	unsigned long SNEPConnectionStartTime = GetSysTicks();
	// SNEP service must be at least in IDLE state
	while (SNEP_GetConnectionState() < SNEP_STATE_IDLE)
	{
		if (GetSysTicks() - SNEPConnectionStartTime > 500)
			return;
	}
	// Receive all NDEF messages as long as a NFC connection is established
	while (true)
	{
		uint32_t MessageSize;
		byte Message[4096];
		// Wait for a incoming NDEF message or loss of connection
		while (!SNEP_TestMessage(&MessageSize))
		{
			if (SNEP_GetConnectionState() < SNEP_STATE_IDLE)
				return;
		}
		// A NDEF message was announced. Now read it.
		int FragmentOffset,FragmentSize;
		for (FragmentOffset = 0; FragmentOffset < MessageSize; FragmentOffset += FragmentSize)
		{
			// Wait, till fragment of the message arrives
			do
			{
				if (SNEP_GetConnectionState() < SNEP_STATE_IDLE)
					return;
				FragmentSize = SNEP_GetFragmentByteCount(DIR_IN);
			}
			while (FragmentSize == 0);
			SNEP_ReceiveMessageFragment(&Message[FragmentOffset],FragmentSize);
		}
		// We read the entire NDEF message

		// Give feedback to the user
		BeepHigh();
		// Turn off the green LED
		LEDOff(GREENLED);
		// Let the red one blink, start with on-state
		LEDOn(REDLED);
		LEDBlink(REDLED,500,500);
		
		// Extract first record from that NDEF message. Ignore other records.
		TNDEFRecord Record;
		NDEF_GetRecord(Message, &Record);
		
		// Send that first record to the host
		int i;
		for (i=0; i<Record.PayloadLength; i++)
			HostWriteChar(Record.Payload[i]);
		HostWriteChar('\r');
	}
}

// ******************************************************************

int main(void)
{
	// Show the startup message
    if (GetHostChannel() == CHANNEL_COM1)
    {
        // A V24 device is writing the version at startup
        HostWriteVersion();
        HostWriteChar('\r');
    }
	// Adjust parameters   
	ConfigSetParameters();
	// Set tag types
	ConfigSetTagTypes();
    	
    // Init LEDs
    LEDInit(REDLED | GREENLED);
    // Turn on green LED
    LEDOn(GREENLED);
    // Turn off red LED
    LEDOff(REDLED);
    // Make some noise at startup at low volume
    SetVolume(30);
    BeepLow();
    BeepHigh();
    // Continue with maximum  volume
    SetVolume(100);
    
    // No transponder found up to now
    LastTagType = NOTAG;

	// Initialize SNEP service
	SNEP_Init();

	while (true)
    {
        // Search a transponder
        if (SearchTag(&TagType,&IDBitCnt,ID,sizeof(ID)))
        {
			if (TagType == HFTAG_NFCP2P)
			{
				ReceiveNDEFMessages();
			}
			else if (ConfigTestIDLength())
			{
				// Is this transponder new to us?
				if (TagType != LastTagType || IDBitCnt != LastIDBitCnt || !CompBits(ID,0,LastID,0,MAXIDBITS))
				{
					// Save this as known ID, before modifying the ID for proper output format
					CopyBits(LastID,0,ID,0,MAXIDBITS);
					LastIDBitCnt = IDBitCnt;
					LastTagType = TagType;
                
					// Yes! Sound a beep
					BeepHigh();
					// Turn off the green LED
					LEDOff(GREENLED);
					// Let the red one blink, start with on-state
					LEDOn(REDLED);
					LEDBlink(REDLED,500,500);

					ConfigModifyID();
					ConfigWriteID();
					HostWriteChar('\r');
				}
			}
			// Start a timeout of two seconds
			StartTimer(2000);
        }
        if (TestTimer())
        {
            LEDOn(GREENLED);
            LEDOff(REDLED);
            LastTagType = NOTAG;
        }
    }
}
