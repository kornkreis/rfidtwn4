// ******************************************************************
//
// Purpose:
//
// App for TWN4, which implements the "Simple Protocol".
//
// V1:
// ------
// - Initial release
//
// V1.01: 
// ------
// - Changed version numbering scheme
// - Split App into several modules
//
// V1.02:
// ------
// - Abstraction layer for support of binary modes
//
// V1.03:
// ------
// - Support optional use of CRC
// - Renamed functions for simple protocol
// - Split protocol handler into TestCommand/ExecuteCommand/SendResponse
// - External access to received messages
// 
// V1.04:
// ------
// - System functions as provided by firmware V1.64
//
// ******************************************************************
//
//      Reference messages for function GetUSBType
//
//      -------------------------------------------------------------------------------
//      Mode      CRC     Command (Host -> TWN4)          Response (TWN4 -> Host)
//      -------------------------------------------------------------------------------
//      ASCII     Off     "0005\r"                        "0001\r"
//      ASCII     On      "000515A7\r"                    "000131E1\r"
//      Binary    Off     0x02 0x00 0x00 0x05             0x02 0x00 0x00 0x01
//      Binary    On      0x04 0x00 0x00 0x05 0x15 0xA7   0x04 0x00 0x00 0x01 0x31 0xE1
//   
//      Reference implementations of CRC algorithm
//      -------------------------------------------------------------------------------
//      Language C
//      ----------
//
//  	uint16_t UpdateCRC(uint16_t CRC,byte Byte)
//  	{
//      	// Update CCITT CRC (reverse polynom 0x8408)
//      	Byte ^= (byte)CRC;
//      	Byte ^= (byte)(Byte << 4);
//      	return (uint16_t)(((Byte << 8) | (CRC >> 8)) ^ (Byte >> 4) ^ (Byte << 3));
//  	}
//
//      Language C#
//      -----------
//
//      ushort UpdateCRC(ushort CRC,byte Byte)
//      {
//      	// Update CCITT CRC (reverse polynom 0x8408)
//          Byte ^= (byte)CRC;
//          Byte ^= (byte)(Byte << 4);
//          return (ushort)(((Byte << 8) | (CRC >> 8)) ^ (Byte >> 4) ^ (Byte << 3));
//      }
//
// ******************************************************************

#include "twn4.sys.h"
#include "apptools.h"
#include "prs.h"

#define MAXIDBYTES  10
#define MAXIDBITS   (MAXIDBYTES*8)

byte ID[MAXIDBYTES];
int IDBitCnt;
int TagType;

byte LastID[MAXIDBYTES];
int LastIDBitCnt;
int LastTagType;

const unsigned char AppManifest[] = { EXECUTE_APP, 1, EXECUTE_APP_ALWAYS, TLV_END };



// If necessary adjust default parameters
#ifndef __APP_CONFIG_H__
    #define LFTAGTYPES        		0
    #define HFTAGTYPES        		0
    #define BIT_LENGTHS       		0xFFFFFFFFFFFFFFFF
    #define SWAP_BITS         		0
    #define SWAP_BYTES        		0
    #define OUTPUT_ALL_BITS   		1
    #define BITOFFSET         		0
    #define BITCOUNT          		0
    #define OUTPUT_RADIX      		16
    #define OUTPUT_DIGITS     		0
    #define ICLASS_READMODE_VALUE  	ICLASS_READMODE_UID
#endif

// Modify ID as configured in AppBlaster
void ConfigModifyID(void)
{
	if (SWAP_BITS)
    	SwapBits(ID,0,IDBitCnt);
	if (SWAP_BYTES)
    	SwapBytes(ID,(IDBitCnt+7)/8);
	if (!OUTPUT_ALL_BITS)
	{
    	CopyBits(ID,0,ID,BITOFFSET,BITCOUNT);
    	IDBitCnt = BITCOUNT;
    }
}
// Send ID to host as configured in AppBlaster
void ConfigWriteID(void)
{
	if (OUTPUT_DIGITS == 0 && OUTPUT_RADIX == 2)
	{
	    // Make number of digits identical with number of digits
	    HostWriteRadix(ID,IDBitCnt,IDBitCnt,OUTPUT_RADIX);
	}
	else if (OUTPUT_DIGITS == 0 && OUTPUT_RADIX == 16)
	{
	    // Make number of digits always a multiple of two (= one byte),
	    // where all bits fit into
	    HostWriteRadix(ID,IDBitCnt,(IDBitCnt+7)/8*2,OUTPUT_RADIX);
	}
	else
	    HostWriteRadix(ID,IDBitCnt,OUTPUT_DIGITS,OUTPUT_RADIX);
}

/*
void SendIDtoHost(void)
{   
	char buffer[255];
    unsigned char chksum = 0;
	int n = MAXIDBYTES;
    va_list vl;

    buffer[0]='#';
    buffer[1]=n+1;
    buffer[2]=cmd;
    chksum=cmd;
    va_start(vl,n);

    for(int i=0;i<n;i++)
        chksum+=buffer[3+i]=va_arg(vl,unsigned int);
    buffer[n+3]=chksum;
    buffer[n+4]='\r';
	
    for(int i=0;i<(n+5);i++)
        serialPort->write(&buffer[i],1);
	
    return n+5;
}*/

// ******************************************************************
// ****** Main Program **********************************************
// ******************************************************************

int main(void)
{
	
	byte answer;
	// Init LEDs
    LEDInit(REDLED | GREENLED);
    GPIOConfigureOutputs(GPIO2|GPIO3,GPIO_PUPD_PULLDOWN,GPIO_OTYPE_PUSHPULL);
	GPIOClearBits(GPIO2|GPIO3);
    SetVolume(50);
	
	I2CInit(I2CMODE_SLAVE | 0x30 | I2CMODE_CHANNEL);
	SetHostChannel(CHANNEL_I2C);
	
	BeepHigh();
	BeepLow();
	LEDOn(REDLED);
	
	int byteCount;
	answer = 0;
    // Main loop
    while (true)
    {
		//LEDOff(REDLED);
		LEDOff(GREENLED);
		
		if (answer == 0)
		{
			LEDOff(REDLED);
			LEDOff(GREENLED);
			if (SearchTag(&TagType,&IDBitCnt,ID,sizeof(ID)))
			{
				if (TagType != LastTagType || IDBitCnt != LastIDBitCnt || !CompBits(ID,0,LastID,0,MAXIDBITS))
				{
					LEDOn(REDLED);
					
					// to clean host-buffer
					while (HostTestByte())
						answer = HostReadByte();
					
					
					CopyBits(LastID,0,ID,0,MAXIDBITS);
					LastIDBitCnt = IDBitCnt;
					LastTagType = TagType;				
					
					ConfigModifyID();
					ConfigWriteID();			
					
					GPIOSetBits(GPIO2|GPIO3);
					BeepHigh();
					
					while (!HostTestByte()){}
					answer = HostReadByte();
					
					if (answer == 5)
					{
						LEDOn(GREENLED);
						GPIOClearBits(GPIO2|GPIO3);
						BeepLow();
						answer = 0;
					}
					else if (answer == 4)
					{
						LEDOn(REDLED);
						BeepHigh();
						BeepHigh();
					}
					else
					{
						for(int i = 0; i < answer; i++)
						{
							BeepHigh();
							StartTimer(500);
							while(!TestTimer()){}
						}
					}
					
					// to clean host-buffer
					//while (HostTestByte())
					//	answer = HostReadByte();
				
				
				}
					
			}
		}
		else
			Reset();
		
    }
}





































