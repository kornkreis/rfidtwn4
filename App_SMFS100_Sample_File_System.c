// ******************************************************************
//
//    File: App_SMFS100_Sample_File_System.c
//    Date: 2014-09-12
// Version: 1.00
//
// Purpose:
//
// Show some basic operations on the storage and file system of TWN4.
// This sample is showing, how storing some kind of configuration
// could be implemented.
//
// In order to test the program, choose CDC as USB and connect to TWN4
// with a terminal program.
// 
// Feel free to modify this program for your specific purposes!
//
// V1.00:
// ------
// - Initial release
//
// ******************************************************************

#include "twn4.sys.h"
#include "apptools.h"

// This is the file ID, under which the file can be found in the directory:
#define CONFIG_FILEID	42

// Implement a minimum header for the configuration
#define CONFIG_VERSION	1

// This is the structure, which holds our imaginary configuration. It
// is stored 1:1 on the file system.
typedef struct __attribute__((__packed__))
{
	byte Version;
	int Baudrate;
	byte LoginData[6];
	byte MoreStuff[20];
} TConfig;

// The default configuration, which is used, once the App starts up
// the first time
const TConfig DefaultConfig =
{
	.Version = CONFIG_VERSION,
	.Baudrate = 115200,
	.LoginData = { 0x12,0x34,0x56,0x78,0x12,0x34 },
	.MoreStuff =
	{
		0x12,0x34,0x56,0x78,0x90,0x12,0x34,0x56,0x78,0x90,
		0x12,0x34,0x56,0x78,0x90,0x12,0x34,0x56,0x78,0x90,
	}
};

// The storage in memory, once configuration is read from a file
static TConfig Config;

// ******************************************************************

bool ReadConfig(TConfig *Config)
{
	// ------ Open File ---------------------------------------------
	
	HostWriteString("Open file for read: ");
    if (!FSOpen(FILE_ENV0,SID_INTERNALFLASH,CONFIG_FILEID,FS_READ))
    {
		HostWriteString("Failed!\r\n");
		return false;
    }
	HostWriteString("OK\r\n");
	
	// ------ Read Data ---------------------------------------------
	
	HostWriteString("Read data: ");
	int BytesRead;
    if (!FSReadBytes(FILE_ENV0,Config,sizeof(TConfig),&BytesRead))
    {
		HostWriteString("Failed!\r\n");
		// File must be closed even in case of an error
		FSClose(FILE_ENV0);
		return false;
    }
	HostWriteString("OK\r\n");
    // Save error for later evaluation
    int ReadError = GetLastError();

	// ------ Close File --------------------------------------------
	
	HostWriteString("Close file: ");
	if (!FSClose(FILE_ENV0))
	{
		HostWriteString("Failed!\r\n");
		return false;
	}
	HostWriteString("OK\r\n");
    
	// ------ Basic Checks ------------------------------------------
	
	HostWriteString("Checking config: ");
	if (ReadError != ERR_ENDOFFILE)
	{
		HostWriteString("File is too long!\r\n");
		return false;
	}
	if (BytesRead < sizeof(TConfig))
	{
		HostWriteString("File is too short!\r\n");
		return false;
	}
	if (Config->Version != CONFIG_VERSION)
	{
		HostWriteString("Incompatible configuration!\r\n");
		return false;
	}
	HostWriteString("OK\r\n");

	// ------ Done --------------------------------------------------
	
    return true;
}

bool WriteConfig(TConfig *Config)
{
	// ------ Open File ---------------------------------------------
	
	HostWriteString("Open file for write: ");
    if (!FSOpen(FILE_ENV0,SID_INTERNALFLASH,CONFIG_FILEID,FS_WRITE))
    {
		HostWriteString("Failed!\r\n");
		return false;
    }
	HostWriteString("OK\r\n");

	// ------ Write Data --------------------------------------------
	
	HostWriteString("Write data: ");
	int BytesWritten;
    if (!FSWriteBytes(FILE_ENV0,Config,sizeof(TConfig),&BytesWritten))
    {
		HostWriteString("Failed!\r\n");
		// File must be closed even in case of an error
		FSClose(FILE_ENV0);
		return false;
    }
	HostWriteString("OK\r\n");
	
	// ------ Close File --------------------------------------------
	
	HostWriteString("Close file: ");
	if (!FSClose(FILE_ENV0))
	{
		HostWriteString("Failed!\r\n");
		return false;
	}
	HostWriteString("OK\r\n");
	
	// ------ Done --------------------------------------------------
	
    return true;
}

bool DeleteConfig(void)
{
	HostWriteString("Delete file: ");
	if (!FSDelete(SID_INTERNALFLASH,CONFIG_FILEID))
	{
		HostWriteString("Failed!\r\n");
		return false;
	}
	HostWriteString("OK\r\n");
	return true;
}

bool StartupConfig(void)
{
	HostWriteString("Mounting storage: ");
	if (!FSMount(SID_INTERNALFLASH,FS_MOUNT_READWRITE))
	{
		HostWriteString("No file system was found!\r\n");
		HostWriteString("Create file system: ");
		if (!FSFormat(SID_INTERNALFLASH,FS_FORMATMAGICVALUE))
		{
			HostWriteString("Format Error!\r\n");
			return false;
		}
		HostWriteString("OK\r\n");
	}
	HostWriteString("OK\r\n");

	if (!ReadConfig(&Config))
	{
		Config = DefaultConfig;
		if (!WriteConfig(&Config))
			return false;
	}
	return true;
}

void HostDumpConfig(TConfig *Config)
{
	byte *Data = (byte*)Config;
	for (int Length = sizeof(TConfig); Length > 0; )
	{
		int BlockLength = MIN(Length,16);
		HostWriteHex(Data,BlockLength*8,BlockLength*2);
		HostWriteString("\r\n");
		Data += BlockLength;
		Length -= BlockLength;
	}
}

int main(void)
{
	// Wait for any key from host before starting up
	HostWriteString("Hit a key to start: ");
    HostReadChar();
    // Prepare file system and read configuration
	StartupConfig();
	HostWriteString("\r\n");
	HostWriteString("Please choose wisely:\r\n");
	HostWriteString(" 1: Write Config\r\n");
	HostWriteString(" 2: Read Config\r\n");
	HostWriteString(" 3: Delete Config\r\n");
	HostWriteString(" 4: Dump Config\r\n");
	while (true)
	{
		HostWriteChar('>');
    	char Char = HostReadChar();
		HostWriteChar(Char);
		HostWriteString("\r\n");
    	switch (Char)
    	{
    	case '1':
			WriteConfig(&Config);
    		break;
    	case '2':
			ReadConfig(&Config);
    		break;
    	case '3':
			DeleteConfig();
    		break;
    	case '4':
			HostDumpConfig(&Config);
    		break;
	    }
	}
}
